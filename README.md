# Mein ioly Kochbuch #

Mein persönliches ioly Kochbuch

### Zweck dieses Kochbuchs ###

enhält Rezepte,

* die nicht im Standard-Kochbuch sind
* von privaten Modulen

### Verwendung ###

* ioly Connector für OXID installieren
* in den Modul-Einstellungen / ioly Cookbook URL die Zeile  
`meinioly => https://bitbucket.org/jobarthel/mein-ioly/get/master.zip` hinzufügen

### Fragen / Wünsche ? ###

* am besten als Ticket [hier](https://bitbucket.org/jobarthel/mein-ioly/issues?status=new&status=open) eintragen